% Laboratorio AIDV - 2 - Esercizio su volumi, segmentazione e validazione

%% Load volume 
meta = analyze75info('IBSR_01_ana.hdr')


X = analyze75read(meta);
% if you want to smooth (filter) volume data use the function smooth3

V = X(51:200,51:200,:); % crop the volume removing border. do the same with the ground truth volume
Y = uint8(X(:,:,51)); %Select a slice for visualization
V(V>255)=255;
V=smooth3(V)

imshow(Y,[]); % visualization
waitforbuttonpress

%%

% compute the histogram of the whole cropped volume and plot it

%% Use matlab kmeans or other functions for clustering: transform image in a vector or array of features, cluster, 
%% then reshape

% % LABELS = ...?
% OUT = reshape(LABELS, size(X));
% Y = uint8(OUT(:,:,51));
% imshow(Y,[])

%% Self implemented k-means. Look at how it is simple!

lev = [30,60,90,120]; % intialize centroids (critical!)
d = size(V)
L = zeros(d(1),d(2),d(3));
a=100;
while a > 0.01 ;   
L = 1 * (V > 0.5*(lev(1)+lev(2)));
L = L + (V > 0.5*(lev(2)+lev(3)));
L = L + (V > 0.5*(lev(3)+lev(4)));
Y = L(:,:,51);
imshow(Y,[]);
a = lev(1) - mean(V(L==0)) + lev(2) - mean(V(L==1)) + lev(3) - mean(V(L==2)) + lev(4) - mean(V(L==3))
lev(1) = mean(V(L==0));
lev(2) = mean(V(L==1));
lev(3) = mean(V(L==2));
lev(4) = mean(V(L==3));
%waitforbuttonpress
end
%% can we add a step of MRF-like optimization? Or can we add a morphological post processing? Try your best!
%morphological
B2=L==2;
B2C=imclose(B2,ones([3 3 3]));


%% Compare segmentation with ground truth

meta = analyze75info('IBSR_01_segTRI_ana.hdr')
LT = analyze75read(meta);
LT = LT(51:200,51:200,:); 

LTS = LT(:,:,51);
figure, imshow(LTS);
waitforbuttonpress

% compute jaccard index
%
L(LT==0)=0;
JI= sum(sum(sum((L==2 & LT==2))))/sum(sum(sum((L==2 | LT==2))))
B2C(LT==0)=0;
LN(LT==0)=0;
JI= sum(sum(sum((B2C & LT==2))))/sum(sum(sum((B2C | LT==2))))
JI= sum(sum(sum((LN==2 & LT==2))))/sum(sum(sum((LN==2 | LT==2))))
